namespace LinqQuerySpecification.Filters
{
  using System;
  using System.Linq;
  using System.Linq.Expressions;
  using LinqQuerySpecification.Utils;

  /// <summary>
  ///   Class for filtering attributes with <see cref="string" /> type.
  /// </summary>
  /// <remarks>
  ///   It can be added to a criteria class as a member, to support the following query parameters:
  ///   <code>
  ///    fieldName.EqualTo = 'something'
  ///    fieldName.NotEqualTo = 'something'
  ///    fieldName.Specified = true  // or false
  ///    fieldName.In = 'something','other'
  ///    fieldName.Contains = 'thing'
  ///    fieldName.DoesNotContain = 'thing'
  ///  </code>
  /// </remarks>
  public class StringFilter : Filter<string>
  {
    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the case-insensitive matching needs to happen.
    /// </summary>
    public string Contains { get; set; }

    /// <summary>
    ///   Gets or sets the property to hold the value of the field where the case-insensitive negative matching needs to happen.
    /// </summary>
    public string DoesNotContain { get; set; }

    /// <summary>
    ///   Applies the filter logic looping through each of the filters that have been set.
    /// </summary>
    /// <remarks>
    ///   Applies the sql-AND condition across each of the specified filters.
    /// </remarks>
    /// <typeparam name="T">The type of the field to which this instance of <c>Filter</c> is applicable to.</typeparam>
    /// <param name="queryable">The pre-existing queryable instance.</param>
    /// <param name="propertyName">The name of the property of <typeparamref name="T" /> for which the filter conditions needs to be applied to.</param>
    /// <returns>The <paramref name="queryable" /> with new linq expressions added.</returns>
    public new IQueryable<T> Apply<T>(IQueryable<T> queryable, string propertyName)
    {
      queryable = base.Apply(queryable, propertyName);

      if (queryable.Any())
      {
        var linqFunctionParameter = Expression.Parameter(typeof(T));
        var modelProperty = Expression.Property(linqFunctionParameter, propertyName);

        queryable = ApplyContains(queryable, linqFunctionParameter, modelProperty);
        queryable = ApplyDoesNotContain(queryable, linqFunctionParameter, modelProperty);
      }

      return queryable;
    }

    /// <summary>Determines whether the specified object is equal to the current object.</summary>
    /// <param name="obj">The object to compare with the current object.</param>
    /// <returns>
    ///   <see langword="true" /> if the specified object is equal to the current object; otherwise, <see langword="false" />.
    /// </returns>
#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    public override bool Equals(object? obj)
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    {
      if (this == obj)
      {
        return true;
      }

      if (obj == null || GetType() != obj.GetType())
      {
        return false;
      }

      if (!base.Equals(obj))
      {
        return false;
      }

      var that = (StringFilter)obj;
      return Objects.AreEqual(Contains, that.Contains) &&
             Objects.AreEqual(DoesNotContain, that.DoesNotContain);
    }

    /// <summary>Serves as the default hash function taking into account all properties on this class.</summary>
    /// <returns>A hash code for the current object.</returns>
    public override int GetHashCode() => HashCode.Combine(base.GetHashCode(), Contains, DoesNotContain);

    /// <summary>Returns a string that represents the current object.</summary>
    /// <returns>A string that represents the current object.</returns>
    public override string ToString() => GetFilterName() + " ["
                                                         + (EqualTo != null ? "EqualTo=" + EqualTo + ", " : string.Empty)
                                                         + (NotEqualTo != null ? "NotEqualTo=" + NotEqualTo + ", " : string.Empty)
                                                         + (Specified != null ? "Specified=" + Specified : string.Empty)
                                                         + (In != null ? "In=" + In : string.Empty)
                                                         + (Contains != null ? "Contains=" + Contains + ", " : string.Empty)
                                                         + (DoesNotContain != null ? "DoesNotContain=" + DoesNotContain + ", " : string.Empty)
                                                         + "]";

    private IQueryable<T> ApplyContains<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, MemberExpression modelProperty)
    {
      if (Contains != null && Contains.GetType().IsAssignableFrom(typeof(string)) && !string.IsNullOrEmpty(Contains))
      {
        var method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        var expression = Expression.Call(modelProperty, method, Expression.Constant(TryConvertToStringLower(Contains)));
        queryable = AddWhere(queryable, linqFunctionParameter, expression);
      }

      return queryable;
    }

    private IQueryable<T> ApplyDoesNotContain<T>(IQueryable<T> queryable, ParameterExpression linqFunctionParameter, MemberExpression modelProperty)
    {
      if (DoesNotContain != null && DoesNotContain.GetType().IsAssignableFrom(typeof(string)) && !string.IsNullOrEmpty(DoesNotContain))
      {
        var method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        var expression = Expression.Not(Expression.Call(modelProperty, method, Expression.Constant(TryConvertToStringLower(DoesNotContain))));
        queryable = AddWhere(queryable, linqFunctionParameter, expression);
      }

      return queryable;
    }
  }
}
