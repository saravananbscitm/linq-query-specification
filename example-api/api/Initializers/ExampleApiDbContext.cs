namespace ExampleApi.Initializers
{
  using System;
  using System.Diagnostics.CodeAnalysis;
  using ExampleApi.Models;
  using Microsoft.EntityFrameworkCore;

  [ExcludeFromCodeCoverage]
  public class ExampleApiDbContext : DbContext
  {
    public ExampleApiDbContext(DbContextOptions<ExampleApiDbContext> options)
      : base(options)
    {
    }

    public ExampleApiDbContext()
    {
    }

    public virtual DbSet<License> Licenses { get; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);

      foreach (var entity in builder.Model.GetEntityTypes())
      {
        // Remove 'AspNet' prefix E.g. AspNetRoleClaims -> RoleClaims
        entity.SetTableName(entity.GetTableName().Replace("AspNet", string.Empty, StringComparison.InvariantCulture));
      }
    }
  }
}
