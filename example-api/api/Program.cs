namespace ExampleApi
{
  using System.Diagnostics.CodeAnalysis;
  using ExampleApi.Initializers;
  using Microsoft.EntityFrameworkCore;
  using Microsoft.Extensions.DependencyInjection;
  using Microsoft.Extensions.Hosting;
  using Microsoft.Extensions.Logging;

  [ExcludeFromCodeCoverage]
  public static class Program
  {
    public static void Main(string[] args)
    {
      var host = CreateHostBuilder(args).Build();
      using (var scope = host.Services.CreateScope())
      {
        var dbContext = scope.ServiceProvider.GetService<ExampleApiDbContext>();
        dbContext.Database.SetCommandTimeout(400);
        dbContext.Database.Migrate();
      }

      host.Run();
    }

    private static IHostBuilder CreateHostBuilder(string[] args) =>
      Host.CreateDefaultBuilder(args)
        .ConfigureLogging(logging =>
        {
          logging.ClearProviders();

          // Log to console (stdout) - in production stdout will be written to /var/log/{{app_name}}.out.log
          logging.AddConsole();
          logging.AddDebug();
        });
  }
}
