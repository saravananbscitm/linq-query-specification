namespace ExampleApi.Controllers
{
  using System.Collections.Generic;
  using ExampleApi.Models;
  using ExampleApi.QueryCriterias;
  using ExampleApi.Services;
  using Microsoft.AspNetCore.Mvc;

  public class LicensesController : ControllerBase
  {
    public LicensesController(ILicenseService licenseService)
    {
      LicenseService = licenseService;
    }

    private ILicenseService LicenseService { get; }

    // GET api/licenses
    [HttpGet]
    public ActionResult<List<License>> Index([FromQuery] LicenseCriteria criteria)
    {
      var licenses = LicenseService.FindAll(criteria);
      return Ok(licenses);
    }
  }
}
