namespace ExampleApi.QueryCriterias
{
  using System;
  using System.Linq;

  public abstract class AbstractCriteria
  {
    protected static void CheckNotNull(IQueryable queryable, string name)
    {
      if (queryable == null)
      {
        throw new ArgumentNullException($"{name} is null");
      }
    }
  }
}
