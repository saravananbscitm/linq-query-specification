namespace ExampleApi.QueryCriterias
{
  using System.Linq;
  using ExampleApi.Models;
  using LinqQuerySpecification.Filters;

  public class LicenseCriteria : AbstractCriteria
  {
    public LongFilter Id { get; set; }

    public StringFilter Name { get; set; }

    public StringFilter Description { get; set; }

    public BooleanFilter IsActive { get; set; }

    public DateFilter CreatedAt { get; set; }

    public IQueryable<License> AddWhere(IQueryable<License> licenses)
    {
      CheckNotNull(licenses, nameof(licenses));

      licenses = Id == null ? licenses : Id.Apply(licenses, nameof(License.Id));
      licenses = Name == null ? licenses : Name.Apply(licenses, nameof(License.Name));
      licenses = Description == null ? licenses : Description.Apply(licenses, nameof(License.Description));
      licenses = IsActive == null ? licenses : IsActive.Apply(licenses, nameof(License.IsActive));
      licenses = CreatedAt == null ? licenses : CreatedAt.Apply(licenses, nameof(License.CreatedAt));

      return licenses;
    }
  }
}
