namespace ExampleApi.Services
{
  using System.Collections;
  using System.Linq;
  using ExampleApi.Initializers;
  using ExampleApi.Models;
  using ExampleApi.QueryCriterias;

  public class LicenseService : ILicenseService
  {
    private readonly ExampleApiDbContext _context;

    public LicenseService(ExampleApiDbContext context) => _context = context;

    public IEnumerable FindAll(LicenseCriteria criteria)
    {
      var licenses = _context.Licenses.AsQueryable<License>();
      if (criteria != null)
      {
        licenses = criteria.AddWhere(licenses);
      }

      return licenses;
    }
  }
}
