namespace ExampleApi.Services
{
  using System.Collections;
  using ExampleApi.QueryCriterias;

  public interface ILicenseService
  {
    IEnumerable FindAll(LicenseCriteria criteria);
  }
}
