namespace ExampleApi.Models
{
  using System;
  using System.ComponentModel.DataAnnotations;

  public class License
  {
    public long Id { get; set; }

    [Required]
    [StringLength(128, MinimumLength = 1)]
    public string Name { get; set; }

    [Required]
    [StringLength(256, MinimumLength = 1)]
    public string Description { get; set; }

    [Required]
    public bool IsActive { get; set; } = true;

    [Required]
    public DateTime CreatedAt { get; set; }
  }
}
