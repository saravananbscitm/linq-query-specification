# LinqQuerySpecification Project

This is the root folder containing all the code for the LinqQuerySpecification project. All instructions found in this file pertain to commands to be run from this directory.

## Setup Pre-requisites

1. Install the following:
  - [.NET Core 3.1](https://www.microsoft.com/net/core)
2. Run the following commands to install the prerequisites:
  ```bash
  dotnet tool install -g dotnet-ef --version 3.1.12
  dotnet tool install -g dotnet-format
  ```


## Example usage

_Help Needed for this section_
This area of the library can use some help. In the meantime, I have created an example project `./example-api/api/exampleApi.csproj` which can give an idea of how to use this package in the Query Specification pattern.

## Running the tests

To run all the unit tests, run this command: `./test-dotnet.sh`

This will run all the tests and publish the test coverage reports to the following directory: `TestResults/Coverage/Reports/index.htm`


## Formatting the code

To format the dotnet code, run the following command: `./format-dotnet.sh`


## Building the code

To build the solution, run the following command: `./build-dotnet.sh`


## Publish nuget package

Compiling the source will create the nuget package. After this, to publish make sure you have the api-key from nuget.org, then run `cd linq-query-specification/api/bin/Debug/ && dotnet nuget push Linq.Query.Filters.0.1.0.nupkg -k <api-key> -s https://api.nuget.org/v3/index.json`
